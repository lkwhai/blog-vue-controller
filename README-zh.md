# whaiblog-vue

> 这里是博客项目的后台管理模块，选择使用了vue-admin-template作为后台管理的模板。

[whaiblog-vue-controller 控制系统Vue](https://gitee.com/lkwhai/blog-vue-controller)

[whaiblog-system 后台系统，基于java](https://gitee.com/lkwhai/whaiblog-system)

[whaiblog-vue-display: 博客展示页面，基于Vue (gitee.com)](https://gitee.com/lkwhai/blog-vue-display)



已经部署在了服务器上，可以进行访问 [whaifree.top](http://whaifree.top)

## 环境

- nodejs
- npm

## dev启动

```bash
# 安装依赖
npm install

# 启动服务
npm run dev
```

浏览器访问 [http://localhost:9528](http://localhost:9528)

## 发布

```bash
# 构建生产环境
npm run build:prod
```



## Contact Me

普通菜鸟一个，望各位大神多多指导！互相学习进步！

<a href= "http://www.whaifree.top">whai的个人博客 whaifree.top 欢迎留言！</a>

<img src="http://img.whaifree.top/image-resp/qrcode1641966581958.jpg">
 </img>




