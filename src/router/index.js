import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'




/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: '/admin',
    component: Layout,
    redirect: '/admin/dashboard',
    meta: {title: 'dashboard', icon: 'dashboard'},
    children: [
      {
        path: 'dashboard',
        name: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        meta: { title: 'dashboard', icon: 'dashboard' }
      }
    ]
  },
  {
    path: '/admin/blog',
    component: Layout,
    redirect: '/admin/blog/blogList',
    meta: {title: '博客', icon: 'blog'},
    children: [
      {
        path: 'blogList',
        name: 'blogList',
        component: () => import('@/views/blog/index'),
        meta: { title: '博客列表', icon: '列表' }
      },
      {
        path: 'blogAdd',
        name: 'blogAdd',
        component: () => import('@/views/blog/addBlog'),
        meta: { title: '博客新增', icon: '编辑' }
      },
      {
        path: 'blogTags',
        name: 'blogTags',
        component: () => import('@/views/tag/index'),
        meta: { title: '博客标签', icon: 'tag' }
      }
    ]
  },
  {
    path: '/admin/user',
    component: Layout,
    redirect: '/admin/user/information',
    meta: {title: '用户信息', icon: 'info'},
    children: [
      {
        path: 'info',
        name: 'info',
        component: () => import('@/views/profile/index'),
        meta: { title: '用户信息', icon: 'info' }
      }
    ]
  },
  {
    path: '/admin/file',
    component: Layout,
    redirect: '/admin/admin/file',
    meta: {title: '文件信息', icon: 'file'},
    children: [
      {
        path: 'file',
        name: 'file',
        component: () => import('@/views/file/file'),
        meta: { title: '文件', icon: 'file' }
      },
      {
        path: 'img',
        name: 'img',
        component: () => import('@/views/file/img'),
        meta: { title: '图片', icon: 'img' }
      }
    ]
  },
  {
    path: '/admin/monitor',
    component: Layout,
    redirect: '/admin/monitor/server',
    meta: {title: '监控信息', icon: 'el-icon-s-tools'},
    children: [
      {
        path: 'browse',
        name: 'browse',
        component: () => import('@/views/monitor/browse'),
        meta: { title: '访问记录', icon: 'browse_fill' }
      },{
        path: 'online',
        name: 'online',
        component: () => import('@/views/blog/index'),
        meta: { title: '在线用户', icon: 'server' }
      },
      {
        path: 'operation',
        name: 'operation',
        component: () => import('@/views/monitor/operation'),
        meta: { title: '操作记录', icon: 'operation' }
      },
      {
        path: 'server',
        name: 'server',
        component: () => import('@/views/monitor/server'),
        meta: { title: '系统服务监控', icon: 'server' }
      },
      {
        path: 'druid',
        name: 'druid',
        component: () => import('@/views/monitor/druid'),
        meta: { title: '数据服务监控', icon: 'druid' }
      }
    ]
  },
  {
    path: '/admin/tools',
    component: Layout,
    redirect: '/admin/tools/swagger',
    meta: {title: '工具', icon: 'swagger'},
    children: [
      {
        path: 'swagger',
        name: 'swagger',
        component: () => import('@/views/tools/swagger'),
        meta: { title: 'swagger', icon: 'swagger' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  // // 当设置 true 的时候该路由不会在侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
  //https://panjiachen.github.io/vue-element-admin-site/zh/guide/essentials/router-and-nav.html#%E9%85%8D%E7%BD%AE%E9%A1%B9
  {
    path: '*',
    redirect: '/404',
    hidden: true
  }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
