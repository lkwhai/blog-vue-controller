import request from "@/utils/request";


export function getServer(data) {
  return request({
    url: '/admin/server/systemInfo',
    method: 'get',
    data
  }).then(
    res => {
      return res.data;
    }
  )
}

/**
 * 操作日志获取
 */
export const operationsGet = (page) => {
  return request({
    url: "/admin/opr/oprList/" + page,
    method: 'get',
  }).then(
    res => {
      return res.data;
    }
  )
};



/**
 * 浏览记录日志获取
 */
export const browseGet = (page) => {
  return request({
    url: "/admin/browse/browseList/"+page,
    method: 'get',
  }).then(
    res => {
      return res.data;
    }
  )
};
