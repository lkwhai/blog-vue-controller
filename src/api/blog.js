import request from "@/utils/request";

/**
 *博客内容
 */

export function blogsGet(data) {
  return request({
    url: '/admin/blog/blogList',
    method: 'get',
    data
  }).then(
    res => {
      return res.data;
    }
  )
}

export function blogsGetWithDelete(page) {
  return request({
    url: '/admin/blog/blogListWithDelete/'+page,
    method: 'get'
  }).then(
    res => {
      return res.data;
    }
  )
}


export function blogsSearch(method,condition) {
  return request({
    url: '/admin/blog/findBlog/'+method+"/"+condition,
    method: 'get',
  }).then(
    res => {
      return res.data;
    }
  )
}

export const blogAdd = (params) => {
  return request({
    url: '/admin/blog/addBlog',
    method: 'post',
    data: params
  }).then(
    res => {
      return res.data;
    }
  )
};



export const commentsGet = (blogId) => {
  return request({
    url: '/blog/blog/'+blogId,
    method: 'get',
  }).then(
    res => {
      return res.data;
    }
  )
};

export const blogSave = (params) => {
  return request({
    url: '/admin/blog/updateBlog',
    method: 'post',
    data: params
  }).then(
    res => {
      return res.data;
    }
  )
};

export const blogDelete = (params) => {
  let url = "/admin/blog/deleteBlog/" + params;

  return request({
    url,
    method: 'delete',
  }).then(
    res=>{
      return res.data
    }
  )
};

export const blogDeleteCancel = (params) => {
  let url = "/admin/blog/deleteBlogCancel/" + params;

  return request({
    url,
    method: 'delete',
  }).then(
    res=>{
      return res.data
    }
  )
};

