import request from "@/utils/request";
import axios from "axios";

/**
 * 文件请求
 */

export const fileSave = (params) => {
  return axios.post(
    process.env.VUE_APP_BASE_API + "/admin/file/uploadFile",
    params,
    {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }
  ).then(res=>res.data)
};

export const fileDelete = (fileId) => {
  return request({
    url: '/admin/file/deleteFile?fileId='+fileId,
    method: 'delete',
  }).then(
    res =>{
      return res.data
    }
  )
};


export const fileDeleteCancel = (fileId) => {
  return request({
    url: '/admin/file/deleteFileCancel?fileId='+fileId,
    method: 'delete',
  }).then(
    res =>{
      return res.data
    }
  )
};


export const fileDeleteAccu = (fileId) => {
  return request({
    url: "/admin/file/deleteFileAccu?fileId="+fileId,
    method: 'delete',
  }).then(
    res => {
      return res.data;
    }
  )
};


// export const filesGet = () => { return axios.get(baseUrl + "/admin/file/fileListWithDelete").then(res => res.data) };

export const fileDownload = (location) => {
  return axios.get(location)
};


export const filesGet = () => {
  return request({
    url: '/admin/file/fileListWithDelete',
    method: 'get',
  }).then(
    res => {
      return res.data;
    }
  )
};
