/* 图片传输请求*/
import request from "@/utils/request";
import axios from "axios";


export const imgGet = () => {
  return request({
    url: "/admin/img/imgList",
    method: "get"
  }).then(res => res.data)
};

export const imgGetWithDelete = () => {
  return request({
    url: "/admin/img/imgListWithDelete",
    method: "get"
  }).then(res => res.data)
};


export const imgSave = (params) => {
  return axios.post(
    process.env.VUE_APP_BASE_API + "/admin/img/addImg",
    params,
    {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }
  ).then(res=>res.data)
};



export const imgDelete = (params) => {
  return request(
    {
      url: "/admin/img/deleteImg?imgId="+params,
      method: 'delete'
    }
  ).then(res => res.data)
  // return req("delete", baseUrl + "/admin/img/deleteImg?imgId="+params)
  // return axios.delete(baseUrl+"/admin/blog/deleteBlog&token=" + localStorage.getItem('logintoken')).then(res => res.data)
};

export const imgDeleteCancel = (params) => {
  return request(
    {
      url: "/admin/img/deleteImgCancel?imgId="+params,
      method: 'delete'
    }
  ).then(res => res.data)
  // return req("delete", baseUrl + "/admin/img/deleteImg?imgId="+params)
  // return axios.delete(baseUrl+"/admin/blog/deleteBlog&token=" + localStorage.getItem('logintoken')).then(res => res.data)
};

export const imgDeleteAccu = (params) => {
  return request(
    {
      url: "/admin/img/deleteImgAccu?imgId="+params,
      method: 'delete'
    }
  ).then(res => {
    return res.data
  })
};
