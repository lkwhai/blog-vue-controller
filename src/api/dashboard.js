import request from "@/utils/request";

export function dashboardInfo() {
  return request({
    url: '/admin/home/dashboardInfo',
    method: 'get'
  }).then(
    res => {
      return res.data;
    }
  )
}
