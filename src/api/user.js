import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/admin/user/user',
    method: 'get',
    params: { token }
  })
}

/*用户*/
export const userGet = (userId) => {
  return request({
    method: "get",
    url: '/admin/user/user?userId='+userId
  }
).then( res => {
  return res.data
})
};


/*用户修改*/
export const userUpdate = (params) => {
  return request({
      method: "post",
      url: "/admin/user/updateUser",
      data: params
    }
  ).then( res => {
    return res.data
  })
};


export function logout() {
  return request({
    url: '/logout',
    method: 'post'
  })
}
