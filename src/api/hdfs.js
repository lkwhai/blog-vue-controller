import request from "@/utils/request";

export function hdfsConfGet() {
  return request({
    url: '/admin/hdfs/hdfsConf',
    method: 'get',
  }).then(
    res => {
      return res.data;
    }
  )
}

export function hdfsListGet(pathWant) {
  return request({
    url: '/admin/hdfs/hdfsList',
    method: 'get',
    config: {
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      }
    },
    params: {
      path: pathWant
    }
  }).then(
    res => {
      return res.data;
    }
  )
}

export function fileBlockLocation(pathWant) {
  return request({
    url: '/admin/hdfs/fileBlockLocation',
    method: 'get',
    config: {
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      }
    },
    params: {
      path: pathWant
    }
  }).then(
    res => {
      return res.data;
    }
  )
}


