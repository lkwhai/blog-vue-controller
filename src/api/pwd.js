import request from "@/utils/request";

export const changePassword = (params) => {
  return request({
    url: '/admin/user/updatePassword',
    method: 'post',
    data: params
  }).then(
    res => {
      return res.data;
    }
  )
};
