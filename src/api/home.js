import request from "@/utils/request";

export function homeGet(homeId) {
  return request({
    url: '/admin/home/getHomePage/'+homeId,
    method: 'get',
  }).then(
    res => {
      return res.data;
    }
  )
}


export function homeSave(para) {
  return request({
    url: '/admin/home/updateHomePage',
    method: 'post',
    data: para
  }).then(
    res => {
      return res.data;
    }
  )
}
