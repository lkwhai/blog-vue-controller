import request from "@/utils/request";

export function verifyCodeGet() {
  return request({
    url: '/captchaImage',
    method: 'get',
  }).then(
    res => {
     return res.data
    }
  )
}

export function verifyCode(para) {

  return request({
    url: '/verifyCode',
    method: 'post',
    data: para
  }).then(
    res => {
      return res.data
    }
  )
}

