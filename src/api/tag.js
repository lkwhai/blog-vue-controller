import request from "@/utils/request";

/**
 * 标签获取
 */
export const tagsGet = () => {
  return request({
    url: '/admin/tag/tagList',
    method: 'get',
  }).then(
    res => {
      return res.data;
    }
  )
};

export const tagSave = (params) => {
  return request({
    url: '/admin/tag/updateTag',
    method: 'post',
    data: params
  }).then(
    res => {
      return res.data;
    }
  )
};

export const tagAdd = (params) => {
  return request({
    url: '/admin/tag/addTag',
    method: 'post',
    data: params
  }).then(
    res => {
      return res.data;
    }
  )

};



export const tagDelete = (params) => {
  return request({
    url: '/admin/tag/deleteTag?tagId=' + params,
    method: 'delete',
  }).then(
    res => {
      return res.data;
    }
  )
};
