import request from "@/utils/request";

export function messageAlert(thisClass,res) {
  if (res.code===0) {
    thisClass.$message({
      type: 'success',
      message: res.msg
    })
  } else {
    thisClass.$message({
      /*上传失败*/
      type: 'error',
      message: res.msg
    })
  }
}
