import request from "@/utils/request";

export function jobsList() {
  return request({
    url: '/admin/job/list',
    method: 'get'
  })
}

export function addJob(params) {
  return request({
    url: '/admin/job/add',
    method: 'post',
    data: params
  })
}

export function updateJob(params) {
  return request({
    url: '/admin/job/update',
    method: 'post',
    data: params
  })
}

export function deleteJobById(jobId) {
  return request({
    url: '/admin/job/delete?jobId=' + jobId,
    method: 'delete'
  })
}

export function changeJobStatus(params) {
  return request({
    url: '/admin/job/changeStatus',
    method: 'post',
    data: params
  })
}
