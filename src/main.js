import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
// if (process.env.NODE_ENV === 'production') {
//   const { mockXHR } = require('../mock')
//   mockXHR()
// }



//vue时间插件
import dayjs from 'dayjs';
Vue.prototype.dayjs = dayjs;
var arraySupport = require("dayjs/plugin/arraySupport");
dayjs.extend(arraySupport);

Vue.filter('dateFormat',function (input) {
  // 使用momentJS这个日期格式化类库实现日期的格式化功能
  return dayjs(input).format("YYYY 年 MM 月 DD 日 HH:mm:ss");
});


//md插件
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
// use

Vue.use(mavonEditor)



// set ElementUI lang to EN
Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
